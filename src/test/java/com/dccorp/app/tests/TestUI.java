package com.dccorp.app.tests;

import com.dccorp.app.controller.UIController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestUI
{
    @Autowired
    private UIController uiController;

    @Test
    public void contextLoads()
    {
        assertThat(uiController).isNotNull();
    }
}
