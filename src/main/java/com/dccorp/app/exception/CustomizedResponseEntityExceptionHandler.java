package com.dccorp.app.exception;

import com.dccorp.app.model.ErrorDetails;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.ui.Model;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Date;

/**
 *
 */
@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler
{
    /**
     *
     * @param ex
     * @return
     */
    @RequestMapping(produces = "application/json")
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ErrorDetails handleAuthorizationException(AccessDeniedException ex)
    {
        return new ErrorDetails( new Date(),ex.getLocalizedMessage(),ex.getMessage(), HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
    }

    /**
     *
     * @param ex
     * @return
     */
    @RequestMapping(produces = "application/json")
    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleBadRequestException(Model model, MissingServletRequestParameterException ex)
    {
        ErrorDetails errorDetails = new ErrorDetails( new Date(),ex.getParameterName(),ex.getMessage(), HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
        model.addAttribute("error",errorDetails);
        return  "error";
    }

    /**
     *
     * @param ex
     * @return
     */
    @RequestMapping(produces = "application/json")
    @ExceptionHandler(HttpClientErrorException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorDetails handleBadRequestExceptionHttpClient(HttpClientErrorException ex)
    {
        return new ErrorDetails( new Date(),ex.getResponseBodyAsString(),ex.getMessage(), HttpStatus.BAD_REQUEST.value(),HttpStatus.BAD_REQUEST.getReasonPhrase());
    }
    /**
     *
     * @param ex
     * @return
     */
    @RequestMapping(produces = "application/json")
    @ExceptionHandler(InvalidGrantException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDetails handleUnsupportedGrantType(InvalidGrantException ex)
    {
        return new ErrorDetails( new Date(),ex.getOAuth2ErrorCode(),ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value(),HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }
}
