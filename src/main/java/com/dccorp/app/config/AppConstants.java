package com.dccorp.app.config;

public class AppConstants
{
    private AppConstants()
    {
    }

    public static final String REPO_STRING ="/repositories/" ;

    public static final String APP_NAME_STRING = "appName";

    public static final String APP_NAME_VAL_STR = "BootstrapiFYDev";
}
