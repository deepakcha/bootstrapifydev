package com.dccorp.app.config;

import com.dccorp.app.properties.BitbucketProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class BitbucketOAuth2ResourceDetails extends AuthorizationCodeResourceDetails
{

    /**
     * @param bitbucketProperties
     */
    @Autowired
    public BitbucketOAuth2ResourceDetails(BitbucketProperties bitbucketProperties)
    {
        setId("bitbucket");
        setUserAuthorizationUri(bitbucketProperties.getOauth2AuthorizationUri());
        setAccessTokenUri(bitbucketProperties.getOauth2AccessTokenUri());
        setClientId(bitbucketProperties.getOauth2ConsumerId());
        setClientSecret(bitbucketProperties.getOauth2ConsumerSecret());
        // The default authentication scheme (`header`) cannot be used since Spring Security OAuth reuses the
        // `token_type` value from the access token response as the `Authorization` header scheme, and the former
        // is lower-case (case-insensitive) whereas the latter is case-sensitive and must be initial-caps
        setAuthenticationScheme(AuthenticationScheme.query);
    }
}
