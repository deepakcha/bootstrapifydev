package com.dccorp.app.config;

import com.dccorp.app.properties.BitbucketProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Configuration
@ComponentScan(basePackageClasses = {BitbucketOAuth2AutoConfiguration.class})
@EnableOAuth2Client
@EnableConfigurationProperties(BitbucketProperties.class)
public class BitbucketOAuth2AutoConfiguration
{
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * @param resourceDetails
     * @param oauth2ClientContext
     * @param jackson2HttpMessageConverter
     * @return
     */
    @Bean
    public OAuth2RestTemplate bitbucketOAuth2RestTemplate(BitbucketOAuth2ResourceDetails resourceDetails,
                                                          OAuth2ClientContext oauth2ClientContext,
                                                          MappingJackson2HttpMessageConverter jackson2HttpMessageConverter,
                                                          RequestResponseLoggingInterceptor requestResponseLoggingInterceptor)
    {
        log.info("invoked BitbucketOAuth2AutoConfiguration:bitbucketOAuth2RestTemplate();");
        ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
        OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(resourceDetails, oauth2ClientContext);
        oAuth2RestTemplate.getMessageConverters().removeIf(MappingJackson2HttpMessageConverter.class::isInstance);
        oAuth2RestTemplate.getMessageConverters().add(jackson2HttpMessageConverter);
        List<ClientHttpRequestInterceptor> interceptors
                = oAuth2RestTemplate.getInterceptors();
        if (CollectionUtils.isEmpty(interceptors))
        {
            interceptors = new ArrayList<>();
        }
        interceptors.add(requestResponseLoggingInterceptor);
        oAuth2RestTemplate.setInterceptors(interceptors);
        oAuth2RestTemplate.setRequestFactory(factory);
        oAuth2RestTemplate.setRetryBadAccessTokens(true);
        return oAuth2RestTemplate;
    }
}
