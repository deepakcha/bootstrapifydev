package com.dccorp.app.repository;

import com.dccorp.app.entity.AppTransaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppRepository extends CrudRepository<AppTransaction, Long>
{
}
