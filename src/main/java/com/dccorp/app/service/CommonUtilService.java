package com.dccorp.app.service;

import com.dccorp.app.entity.AppTransaction;
import com.dccorp.app.model.BitbucketUser;
import com.dccorp.app.repository.AppRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Component
public class CommonUtilService
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AppRepository appRepository;

    public void setCustomAuthenticationObject(BitbucketUser user, String accessToken)
    {
        AppTransaction appTransaction = new AppTransaction(user.getUsername(), accessToken);
        appRepository.save(appTransaction);

        UsernamePasswordAuthenticationToken auth =
                new UsernamePasswordAuthenticationToken(user.getUsername(),
                        accessToken,
                        Arrays.asList(new SimpleGrantedAuthority("ROLE_APP")));
        if (logger.isDebugEnabled())
        {
            logger.info("~ access token value: " + accessToken);
        }
        user.setAccessToken(accessToken);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}
