package com.dccorp.app.service;

import com.dccorp.app.config.AppConstants;
import com.dccorp.app.entity.AppTransaction;
import com.dccorp.app.model.BitbucketUser;
import com.dccorp.app.model.RepositoryResponse;
import com.dccorp.app.model.Values;
import com.dccorp.app.repository.AppRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class BitBucketServiceImpl implements IBitBucketService
{
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OAuth2RestTemplate oAuth2RestTemplate;

    @Value("${atlassian.bitbucket.api}")
    private String bitBucketApiHostURI;

    @Autowired
    private CommonUtilService commonUtilService;



    @PreAuthorize("hasRole('ROLE_APP')")
    @Override
    public String getLastCommit(String repoName) throws IOException
    {
        log.info("invoked BitbucketUserController:getUser()");
        ResponseEntity<String> response = oAuth2RestTemplate.getForEntity(bitBucketApiHostURI + AppConstants.REPO_STRING + repoName + "/commits/develop", String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.readTree(response.getBody());
        String hashCommitVal = actualObj.get("values").get(0).get("hash").toString();
        log.info(hashCommitVal);
        return hashCommitVal;
    }

    @PreAuthorize("hasRole('ROLE_APP')")
    @Override
    public String provisionBranches(String repo, String lastCommitVal, String branchName) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.createObjectNode();
        ((ObjectNode) rootNode).put("name", branchName);
        JsonNode childNode2 = mapper.createObjectNode();
        ((ObjectNode) childNode2).put("hash", lastCommitVal);
        ((ObjectNode) rootNode).set("target", childNode2);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
        log.info(jsonString);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity =
                new HttpEntity<>(jsonString, headers);

        ResponseEntity<String> responseEntity = oAuth2RestTemplate.postForEntity(bitBucketApiHostURI + "/repositories/" + repo + "/refs/branches", entity, String.class);

        JsonNode actualObj = mapper.readTree(responseEntity.getBody());
        return actualObj.get("name").toString();
    }

    @PreAuthorize("hasRole('ROLE_APP')")
    @Override
    public Values[] getRepositories(String teamName)
    {
        ResponseEntity<RepositoryResponse> responseEntity = oAuth2RestTemplate.getForEntity(bitBucketApiHostURI + "/repositories/" + teamName, RepositoryResponse.class);
        RepositoryResponse repositoryResponse = responseEntity.getBody();
        Values[] values = repositoryResponse.getValues();
        for (Values value : values)
        {
            log.info("############");
            log.info(value.getFull_name());
            log.info(value.getScm());
            log.info(value.getSlug());
        }
        return values;
    }

    @Override
    public BitbucketUser getUserInfo()
    {
        BitbucketUser user = oAuth2RestTemplate.getForEntity(bitBucketApiHostURI + "/user", BitbucketUser.class).getBody();
        /* set authentication user object - for user persistance later. */
        if (null != user)
        {
            commonUtilService.setCustomAuthenticationObject(user, oAuth2RestTemplate.getAccessToken().getValue());
        }
        return user;
    }
}
