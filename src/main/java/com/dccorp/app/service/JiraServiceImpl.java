package com.dccorp.app.service;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.domain.input.ComplexIssueInputFieldValue;
import com.atlassian.jira.rest.client.api.domain.input.FieldInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.dccorp.app.jira.AppJiraRestClient;
import com.dccorp.app.model.BitbucketUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class JiraServiceImpl implements IJiraService
{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AppJiraRestClient appJiraRestClient;

    @PreAuthorize("hasRole('ROLE_APP')")
    @Override
    public List<String> createSubTask(String projectKey, String issueKey, String repoName)
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        logger.info("starting subtask creation");
        ArrayList<String> subTaskKeysList = new ArrayList<>();
        Map<String, Object> parent = new HashMap<>();
        parent.put("key", issueKey);
        FieldInput parentField = new FieldInput("parent", new ComplexIssueInputFieldValue(parent));

        IssueRestClient issueClient = appJiraRestClient.getClient().getIssueClient();
        Map<String, String> subTaskMap = appJiraRestClient.getSubTaskMap();
        for (Map.Entry<String, String> entry : subTaskMap.entrySet())
        {
            logger.info("processing {}, {}", entry.getKey(), entry.getValue());
            IssueInput newSubTask = new IssueInputBuilder(projectKey, 10000l, entry.getValue() + " for " + repoName).
                    setFieldInput(parentField).
                    setFieldValue("assignee", ComplexIssueInputFieldValue.with("name", "chaudharydeepak08")).
                    build();
            String subTaskKey = issueClient.createIssue(newSubTask).claim().getKey();

            subTaskKeysList.add(subTaskKey);
            logger.info(subTaskKey);
        }
        return subTaskKeysList;
    }
}
