package com.dccorp.app.service;

import java.util.List;

public interface IJiraService
{

    List<String> createSubTask(String projectKey, String issueKey, String repoName);
}
