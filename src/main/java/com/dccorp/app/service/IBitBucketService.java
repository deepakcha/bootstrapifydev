package com.dccorp.app.service;

import com.dccorp.app.model.BitbucketUser;
import com.dccorp.app.model.Values;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public interface IBitBucketService
{
    String getLastCommit(String repoName) throws IOException;

    String provisionBranches(String repo, String lastCommitVal, String branchName) throws IOException;

    Values[] getRepositories(String teamName) throws IOException;

    BitbucketUser getUserInfo() throws IOException;
}
