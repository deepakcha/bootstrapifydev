package com.dccorp.app.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "bitbucket")
public class BitbucketProperties
{

    private String oauth2AuthorizationUri;
    private String oauth2AccessTokenUri;
    private String oauth2ConsumerId;
    private String oauth2ConsumerSecret;

    /**
     *
     * @return
     */
    public String getOauth2AuthorizationUri()
    {
        return oauth2AuthorizationUri;
    }

    /**
     *
     * @param oauth2AuthorizationUri
     */
    public void setOauth2AuthorizationUri(String oauth2AuthorizationUri)
    {
        this.oauth2AuthorizationUri = oauth2AuthorizationUri;
    }

    /**
     *
     * @return
     */
    public String getOauth2AccessTokenUri()
    {
        return oauth2AccessTokenUri;
    }

    /**
     *
     * @param oauth2AccessTokenUri
     */
    public void setOauth2AccessTokenUri(String oauth2AccessTokenUri)
    {
        this.oauth2AccessTokenUri = oauth2AccessTokenUri;
    }

    /**
     *
     * @return
     */
    public String getOauth2ConsumerId()
    {
        return oauth2ConsumerId;
    }

    /**
     *
     * @param oauth2ConsumerId
     */
    public void setOauth2ConsumerId(String oauth2ConsumerId)
    {
        this.oauth2ConsumerId = oauth2ConsumerId;
    }

    /**
     *
     * @return
     */
    public String getOauth2ConsumerSecret()
    {
        return oauth2ConsumerSecret;
    }

    /**
     *
     * @param oauth2ConsumerSecret
     */
    public void setOauth2ConsumerSecret(String oauth2ConsumerSecret)
    {
        this.oauth2ConsumerSecret = oauth2ConsumerSecret;
    }
}
