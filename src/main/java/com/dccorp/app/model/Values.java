package com.dccorp.app.model;

public class Values
{
    String scm;
    String slug;
    String full_name;
    Project project;

    public String getScm()
    {
        return scm;
    }

    public void setScm(String scm)
    {
        this.scm = scm;
    }

    public String getSlug()
    {
        return slug;
    }

    public void setSlug(String slug)
    {
        this.slug = slug;
    }

    public String getFull_name()
    {
        return full_name;
    }

    public void setFull_name(String full_name)
    {
        this.full_name = full_name;
    }

    public Project getProject()
    {
        return project;
    }

    public void setProject(Project project)
    {
        this.project = project;
    }
}
