package com.dccorp.app.model;

import java.util.Date;

public class ErrorDetails
{
    private Date timestamp;
    private String message;
    private String details;
    private int errorCode;
    private String responePhrase;

    /**
     *
     * @param timestamp
     * @param message
     * @param details
     */
    public ErrorDetails(Date timestamp, String message, String details, int errorCode, String responePhrase)
    {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
        this.errorCode = errorCode;
        this.responePhrase = responePhrase;
    }

    /**
     *
     * @return
     */
    public Date getTimestamp()
    {
        return timestamp;
    }

    /**
     *
     * @param timestamp
     */
    public void setTimestamp(Date timestamp)
    {
        this.timestamp = timestamp;
    }

    /**
     *
     * @return
     */
    public String getMessage()
    {
        return message;
    }

    /**
     *
     * @param message
     */
    public void setMessage(String message)
    {
        this.message = message;
    }

    /**
     *
     * @return
     */
    public String getDetails()
    {
        return details;
    }

    /**
     *
     * @param details
     */
    public void setDetails(String details)
    {
        this.details = details;
    }

    public int getErrorCode()
    {
        return errorCode;
    }

    public void setErrorCode(int errorCode)
    {
        this.errorCode = errorCode;
    }

    public String getResponePhrase()
    {
        return responePhrase;
    }

    public void setResponePhrase(String responePhrase)
    {
        this.responePhrase = responePhrase;
    }
}