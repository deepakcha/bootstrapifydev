package com.dccorp.app.model;

import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class BitbucketUser
{
    private String displayName;
    private String accountId;
    private String username;
    private Links links;
    private String accessToken;

    /**
     * @return
     */
    public Links getLinks()
    {
        return links;
    }

    /**
     * @param links
     */
    public void setLinks(Links links)
    {
        this.links = links;
    }

    /**
     * @return
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * @return
     */
    public String getAccountId()
    {
        return accountId;
    }

    /**
     * @return
     */
    public String getDisplayName()
    {
        return displayName;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    @Override
    public String toString()
    {
        return String.format(getUsername() + " : " + getDisplayName() + " : " + getAccountId() + " : " + getLinks());
    }
}