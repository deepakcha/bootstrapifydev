package com.dccorp.app.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@ConfigurationProperties(prefix = "jira")
@Component
public class JiraUser
{
    String username;
    String password;
    String url;
    private Map<String, String> subtasks;

    public Map<String, String> getSubtasks()
    {
        return subtasks;
    }

    public void setSubtasks(Map<String, String> subtasks)
    {
        this.subtasks = subtasks;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }


}
