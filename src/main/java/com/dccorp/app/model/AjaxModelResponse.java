package com.dccorp.app.model;

import java.util.List;

public class AjaxModelResponse
{
    String repoName;
    String storyNumber;
    String message;
    String lastCommitOnDev;
    String createdBranchName;
    List<String> jiraSubTask;

    public String getRepoName()
    {
        return repoName;
    }

    public void setRepoName(String repoName)
    {
        this.repoName = repoName;
    }

    public String getStoryNumber()
    {
        return storyNumber;
    }

    public void setStoryNumber(String storyNumber)
    {
        this.storyNumber = storyNumber;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getLastCommitOnDev()
    {
        return lastCommitOnDev;
    }

    public void setLastCommitOnDev(String lastCommitOnDev)
    {
        this.lastCommitOnDev = lastCommitOnDev;
    }

    public String getCreatedBranchName()
    {
        return createdBranchName;
    }

    public void setCreatedBranchName(String createdBranchName)
    {
        this.createdBranchName = createdBranchName;
    }

    public List<String> getJiraSubTask()
    {
        return jiraSubTask;
    }

    public void setJiraSubTask(List<String> jiraSubTask)
    {
        this.jiraSubTask = jiraSubTask;
    }

}
