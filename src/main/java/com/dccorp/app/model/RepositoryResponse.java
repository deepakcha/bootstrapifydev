package com.dccorp.app.model;

public class RepositoryResponse
{
    int pagelen;
    Values[] values;
    int page;
    int size;

    public int getPagelen()
    {
        return pagelen;
    }

    public void setPagelen(int pagelen)
    {
        this.pagelen = pagelen;
    }

    public Values[] getValues()
    {
        return values;
    }

    public void setValues(Values[] values)
    {
        this.values = values;
    }

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }


}
