package com.dccorp.app.model;

public class GenericLinks
{
    String href;

    public String getHref()
    {
        return href;
    }

    public void setHref(String href)
    {
        this.href = href;
    }

}
