package com.dccorp.app.model;

public class Links
{
    Avatar avatar;

    public Avatar getAvatar()
    {
        return avatar;
    }

    public void setAvatar(Avatar avatar)
    {
        this.avatar = avatar;
    }
}
