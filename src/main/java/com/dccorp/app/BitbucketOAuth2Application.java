package com.dccorp.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 */
@SpringBootApplication
public class BitbucketOAuth2Application
{
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        new SpringApplication(BitbucketOAuth2Application.class).run(args);
    }
}
