package com.dccorp.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class AppTransaction
{
    @Id
    @GeneratedValue
    private Long id;
    private String username;
    private String token;
    private Date timestamp;

    protected AppTransaction()
    {
    }

    public AppTransaction(String username, String token)
    {
        this.username = username;
        this.token = token;
        this.timestamp = new Date();
    }

    @Override
    public String toString()
    {
        return String.format(
                "AppTransaction[id=%d, firstName='%s', lastName='%s']",
                id, username, token);
    }
}
