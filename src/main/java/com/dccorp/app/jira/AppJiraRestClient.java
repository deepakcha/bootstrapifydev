package com.dccorp.app.jira;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClientFactory;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.dccorp.app.model.JiraUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

@Component
public class AppJiraRestClient
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    JiraUser jiraUser;

    private JiraRestClient client;

    private Map<String, String> subTaskMap;

    @PostConstruct
    public void init()
    {
        try
        {
            logger.info("~ instantialting JiraRestClient. {} {}", jiraUser, jiraUser.getSubtasks().size());
            JiraRestClientFactory restFactory = new AsynchronousJiraRestClientFactory();
            logger.info("~ instantialting JiraRestClient.");
            this.client = restFactory.createWithBasicHttpAuthentication(new URI(jiraUser.getUrl()), jiraUser.getUsername(), jiraUser.getPassword());
            this.subTaskMap = jiraUser.getSubtasks();
        }
        catch (URISyntaxException e)
        {
            logger.error("~ erro occured during instantialting JiraRestClient. {}", e.getMessage());
        }
    }

    public JiraRestClient getClient()
    {
        return client;
    }

    public void setClient(JiraRestClient client)
    {
        this.client = client;
    }

    public Map<String, String> getSubTaskMap()
    {
        return subTaskMap;
    }

    public void setSubTaskMap(Map<String, String> subTaskMap)
    {
        this.subTaskMap = subTaskMap;
    }
}
