package com.dccorp.app.controller;

import com.dccorp.app.config.AppConstants;
import com.dccorp.app.model.AjaxModelResponse;
import com.dccorp.app.model.BitbucketUser;
import com.dccorp.app.model.Values;
import com.dccorp.app.service.CommonUtilService;
import com.dccorp.app.service.IBitBucketService;
import com.dccorp.app.service.IJiraService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.List;

@EnableOAuth2Client
@Controller
public class UIController
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    IBitBucketService appServiceImpl;

    @Autowired
    IJiraService jiraServiceImpl;

    @Autowired
    private BitbucketUser user;

    @Autowired
    private CommonUtilService commonUtilService;

    @GetMapping("/ui")
    public String selectionPage(Model model, @RequestParam("teamName") String teamName) throws IOException
    {
        logger.info("invoking selectionPage");
        user = appServiceImpl.getUserInfo();

        logger.info("user: ", user);
        model.addAttribute("user", user);

        Values[] values = appServiceImpl.getRepositories(teamName);
        model.addAttribute("repoArray", values);

        model.addAttribute(AppConstants.APP_NAME_STRING, AppConstants.APP_NAME_VAL_STR);
        return "index";
    }

    @PostMapping("/bootstrap")
    public ResponseEntity<AjaxModelResponse> goBootStrap(@RequestBody String repoName, @RequestParam("storyid") String branchName,
                                                         @RequestParam("taskid") String taskid) throws IOException
    {
        logger.info("~~~~~~~~~~~~~~~~~~~~~~AjaxModelResponse start~~~~~~~~~~~~~~~~~~~~~~");
        if (null == user)
        {
            logger.info("user is null, initizing from getuserinfo");
            user = appServiceImpl.getUserInfo();
        }
        else
        {
            logger.info("user is not null, settting custom auth object.");
            commonUtilService.setCustomAuthenticationObject(user, user.getAccessToken());
        }

        AjaxModelResponse ajaxModelResponse = new AjaxModelResponse();
        logger.info(repoName);
        ajaxModelResponse.setStoryNumber(branchName);
        branchName = "feature/" + branchName;
        logger.info(branchName);
        ObjectMapper mapper = new ObjectMapper();
        String lastCommitOnDevelopBranch;

        try
        {
            repoName = mapper.readValue(repoName, String.class);
            logger.info("~processing repo: {}", repoName);
            ajaxModelResponse.setRepoName(repoName);
            lastCommitOnDevelopBranch = appServiceImpl.getLastCommit(repoName);
            lastCommitOnDevelopBranch = mapper.readValue(lastCommitOnDevelopBranch, String.class);
            logger.info("~processing repo: {} lastCommitOnDevelopBranch: {} ", repoName, lastCommitOnDevelopBranch);
            ajaxModelResponse.setLastCommitOnDev(lastCommitOnDevelopBranch);
            logger.info(lastCommitOnDevelopBranch);
            String createdBranchName = appServiceImpl.provisionBranches(repoName, lastCommitOnDevelopBranch, branchName);
            createdBranchName = mapper.readValue(createdBranchName, String.class);
            ajaxModelResponse.setCreatedBranchName(createdBranchName);
            ajaxModelResponse.setMessage("SUCCESS");
            logger.info("~processing repo: {} lastCommitOnDevelopBranch: {} createdBranchName: {}", repoName, lastCommitOnDevelopBranch, createdBranchName);
            logger.info("~~ starting jira operations: ");
            List<String> subTaskKeyList = jiraServiceImpl.createSubTask("US", taskid, repoName + "(" + branchName + ")");
            ajaxModelResponse.setJiraSubTask(subTaskKeyList);
        }
        catch (IOException e)
        {
            logger.error(e.getMessage());
            ajaxModelResponse.setMessage(e.getLocalizedMessage());
            return ResponseEntity.badRequest().body(ajaxModelResponse);
        }
        catch (HttpClientErrorException e)
        {
            logger.error(e.getMessage());
            ajaxModelResponse.setMessage(e.getResponseBodyAsString());
            return ResponseEntity.badRequest().body(ajaxModelResponse);
        }
        logger.info("~~~~~~~~~~~~~~~~~~~~~~AjaxModelResponse end~~~~~~~~~~~~~~~~~~~~~~");
        return ResponseEntity.ok(ajaxModelResponse);
    }

}
